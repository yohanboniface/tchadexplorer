$(document).on('pageinit', '#index', function () {

    /* ************** Map init ************** */
    var map = L.map('map', {zoomControl: false});

    var tilelayer = L.tileLayer('http://localhost:20008/tile/hdm/{z}/{x}/{y}.png', {
        maxZoom: 20,
        attribution: "OpenStreetMap Contributors"
    }).addTo(map);

    map.setView([12.1177, 15.0674], 13);

    $(document).on('pageshow', '#index', function () {
        $('#map')[0].style.height = window.innerHeight + "px";
        map.invalidateSize();
        $('#map')[0].style.height = "100%";
    });


    /* ************** Map listeners ************** */
    function onLocationFound(e) {
        var radius = e.accuracy / 2;
        var smallCircle = L.circle(e.latlng, 2).addTo(map);
        var bigCircle = L.circle(e.latlng, radius).addTo(map);
        window.setTimeout(function () {
            smallCircle.remove();
            bigCircle.remove();
        }, 5000);
    }
    map.on('locationfound', onLocationFound);



    /* ************** Buttons listeners ************** */
    $('#locationButton').on('click', function () {
        map.locate({
            maxZoom: 18,
            setView: true,
            timeout: 3000
        });
    });

});